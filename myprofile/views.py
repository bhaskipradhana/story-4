from django.shortcuts import render

def profile(request):
    return render(request, 'Profile.html')

def skills(request):
    return render(request, 'Skills.html')

def experience(request):
    return render(request, 'experience.html')

def contact(request):
    return render(request, 'Contact.html')
    

def quotes(request):
    return render(request, 'Quotes.html')
    
    
# Create your views here.
