from django.urls import path
from . import views

app_name = 'myprofile'

urlpatterns = [
    path('', views.profile, name='profile'),
    path('Skills/', views.skills, name='skills'),
    path('Experience/', views.experience, name='experience'),
    path('Contact/', views.contact, name='contact'),
    path('Quotes/', views.quotes, name='quotes'),
]